#!/usr/bin/env python3

import ev3dev.ev3 as ev3

def clamp(_inp, _min, _max):
    if _inp < _min:
        return _min
    if _inp > _max:
        return _max
    return _inp
# clamp

class PID_controller:
    def __init__(self, _kP: float, _kI: float, _kD: float):
        self.kP = _kP
        self.kI = _kI
        self.kD = _kD
        self.x_last = 0
        self.integral = 0
        self.derivative = 0

    def execute(self, _x):
        self.integral = self.integral + _x
        self.derivative = _x - self.x_last
        res = self.kP * _x + self.kI * self.integral + self.kD * self.derivative
        self.x_last = _x
        return res
#PID_controller

class ev3_controller:
    def __init__(self):
        self.color_sensor_L = ev3.ColorSensor('in1')
        self.color_sensor_R = ev3.ColorSensor('in2')
        self.color_sensor_L.mode = 'COL-REFLECT'
        self.color_sensor_R.mode = 'COL-REFLECT'
        self.motor_L = ev3.LargeMotor('outB')
        self.motor_R = ev3.LargeMotor('outD')
        self.is_verbose = False
        self.COLOR_BLACK = 60
        self.COLOR_WHITE = 5
        self.COLOR_THRESHOLD = (self.COLOR_BLACK + self.COLOR_WHITE) / 2

    def set_duty_cycle(self, _dL: int, _dR: int):
        self.motor_L.duty_cycle_sp = clamp(_dL, -100, 100)
        self.motor_R.duty_cycle_sp = clamp(_dR, -100, 100)

    def print_duty_cycle(self):
        if self.is_verbose:
            print("Motor speed [" + str(self.motor_L.duty_cycle_sp) + "," + str(self.motor_R.duty_cycle_sp) + "]")

    def start_motors(self):
        self.print_duty_cycle()
        self.motor_L.run_direct()
        self.motor_R.run_direct()

    def pid_behaviour(self):
        base_L = self.motor_L.duty_cycle_sp
        base_R = self.motor_R.duty_cycle_sp
        pid = PID_controller(10, 0, 0)
        self.start_motors()
        self.print_duty_cycle()
        while True:
            x = self.color_sensor_L.value() - self.color_sensor_R.value() + self.COLOR_THRESHOLD
            y = pid.execute(x)
            self.set_duty_cycle(int(base_L + y)*-1, int(base_R - y)*-1)
            self.start_motors()

    def __del__(self):
        self.set_duty_cycle(0, 0)
        self.start_motors()
#ev3_controller

if __name__ == "__main__":
    print("Robot Start")
    brick = ev3_controller()
    brick.is_verbose = True
    brick.set_duty_cycle(50, 50)
    brick.pid_behaviour()
    print("Robot End")
